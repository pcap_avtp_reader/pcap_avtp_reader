/*
  pcap_avtp_reader
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#pragma once

#include "config.h"
#include <time.h>

typedef struct timespec timens_t;

timens_t get_time();
timens_t diff_time(timens_t time1, timens_t time2);
void print_time(timens_t time);
