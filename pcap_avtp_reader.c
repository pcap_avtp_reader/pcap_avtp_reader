/*
  pcap_avtp_reader
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <pcap.h>

#include "timens.h"
#include "avtp.h"

#define safe_snprintf(dest, ...) snprintf(dest, sizeof(dest), __VA_ARGS__)

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define DEFAULT_FILTER "(ether proto " STR(ETHTYPE_VLAN) " or ether proto " STR(ETHTYPE_AVTP) ")"
#define DST_MAC_FILTER "(ether dst %s)"
#define MAX_FILTER_LEN 256

typedef char errbuf_t[PCAP_ERRBUF_SIZE];
typedef struct bpf_program filter_t;

#define PCAP_COMPILE_OPTIMIZE 1

void usage()
{
  fprintf(stderr, "\nUsage:\n pcap_avtp_reader pcap-file [destination-mac]\n");
}


inline void print_mac(mac_t mac)
{
  printf("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}


void print_af(avtp_t *avtp)
{
  uint16_t chunk, channel;
  printf("type af\n");

  printf("stream id valid %u\n", avtp->sid_valid);
  printf("seqnr %u\n", avtp->seqnr);
  printf("stream id 0x%016" PRIx64 "\n", avtp->sid);
  printf("freq %u\n", avtp->freq);
  printf("ts valid %u\n", avtp->ts_valid);
  printf("ts %u\n", avtp->ts);
  printf("bits %u\n", avtp->bits);
  printf("channels %u\n", avtp->channels);
  printf("sparse ts %u\n", avtp->sparse_ts);

  printf("format ");
  if (avtp->format == AF_INT16) {
    printf("int16\n");
    for (chunk = 0; chunk < avtp->chunks; chunk++) {
      printf("chunk %u samples:", chunk);
      for (channel = 0; channel < avtp->channels; channel++) {
        printf(" %04x", (uint16_t)avtp->sample_int16[chunk * avtp->channels + channel]);
      }
      printf("\n");
    }
  } else {
    printf("unsupported!");
  }
}


void print_crf(avtp_t *avtp)
{
  uint16_t ts_idx;

  printf("type crf\n");

  printf("seqnr %u\n", avtp->seqnr);
  printf("stream id 0x%016" PRIx64 "\n", avtp->sid);
  printf("freq %u\n", avtp->freq);
  printf("interval %u\n", avtp->crf_interval);

  for (ts_idx = 0; ts_idx < avtp->crf_ts_cnt; ts_idx++) {
    printf("ts[%u] 0x%016" PRIx64 "\n", ts_idx, avtp->crf_ts[ts_idx]);
  }
}


void print_avtp(avtp_t *avtp)
{
  if (!avtp->is_valid) {
    printf("invalid avtp!\n");
    return;
  }

  printf("dst mac "); print_mac(avtp->dstmac) ; printf("\n");    
  printf("src mac "); print_mac(avtp->srcmac) ; printf("\n");    

  if (avtp->has_vlan) {
    printf("vlan id %d\n", avtp->vlan_id);
  }

  if (avtp->type == AVTP_TYPE_AF) {
    print_af(avtp);
  } else if (avtp->type == AVTP_TYPE_CRF) {
    print_crf(avtp);
  } else {
    printf("unsupported avtp type!\n");
  }
}

int main(int argc, char **argv)
{
  pcap_t *pcap;
  avtp_t avtp;
  errbuf_t errbuf;
  filter_t filter;
  packet_t packet;
  header_t header;
  timens_t time1, time2;
  char filter_str[MAX_FILTER_LEN];
  uint64_t packet_cnt = 0;

  // Check the number of arguments
  if ((argc < 2) || (argc > 3)) {
    usage();
    exit(EXIT_FAILURE);
  }

  if (argc == 2) {
    safe_snprintf(filter_str, DEFAULT_FILTER);
  } else {
    safe_snprintf(filter_str, DEFAULT_FILTER " and " DST_MAC_FILTER, argv[2]);
  }

  // Open the PCAP file
  pcap = pcap_open_offline(argv[1], errbuf);

  if (!pcap) {
    fprintf(stderr, "%s\n", errbuf);
    exit(EXIT_FAILURE);
  }

  // Compile the filter
  if (pcap_compile(pcap, &filter, filter_str, PCAP_COMPILE_OPTIMIZE, PCAP_NETMASK_UNKNOWN) == PCAP_ERROR) {
    pcap_close(pcap);
    fprintf(stderr, "Invalid filter %s: %s\n", filter_str, pcap_geterr(pcap));
    exit(EXIT_FAILURE);
  }

  // Apply the filter
  if (pcap_setfilter(pcap, &filter) == PCAP_ERROR) {
    pcap_close(pcap);
    fprintf(stderr, "Filter failed %s: %s\n", filter_str, pcap_geterr(pcap));
    exit(EXIT_FAILURE);
  }

  time1 = get_time();

  while (1) {
    // Read packet
    packet = pcap_next(pcap, &header);

    if (!packet) break;

    // Convert packet data to avtp structure
    packet2avtp(packet, &header, &avtp);

    if (!avtp.is_avtp) continue; // skip VLAN tagged non-AVTP packets

    packet_cnt++;

    // Print the avtp structure
    printf("packet number %lu\n", packet_cnt);
    printf("packet ts %Lf\n", avtp.packet_ts);
    printf("packet size %d\n", avtp.packet_size);
    print_avtp(&avtp);
    printf("---\n");
  };

  time2 = get_time();

  // Close the PCAP file
  pcap_close(pcap);

  // Print stats
  printf("Total time "); print_time(diff_time(time2, time1)); printf("s\n");
  printf("Packets processed %ld\n", packet_cnt);

  exit(EXIT_SUCCESS);
}
