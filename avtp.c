/*
  pcap_avtp_reader
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <stdio.h>
#include <netinet/in.h>
#include "avtp.h"

#if defined(DEBUG)
  #define debug(...) printf(__VA_ARGS__)
#else
  #define debug(...)
#endif

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
  #define htonll(x)   (x)
  #define ntohll(x)   (x)
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
  #define htonll(x)   ((((uint64_t)htonl((x) & 0xffffffff)) << 32) + htonl((x) >> 32))
  #define ntohll(x)   ((((uint64_t)ntohl((x) & 0xffffffff)) << 32) + ntohl((x) >> 32))
#else
  #error "Unknown __BYTE_ORDER__"
#endif

#define VLAN_ID_MASK 0x0fff
#define CRF_BASE_FREQ_MASK 0x0fffffff
#define AF_CHANNELS_MASK 0x03ff
#define AF_SR_SHIFT 12
#define AF_SID_VALID_SHIFT 7
#define CRF_SID_VALID_SHIFT 7
#define AF_TS_VALID_MASK 0x01
#define AF_SPARSE_TS_SHIFT 4
#define AF_SPARSE_TS_MASK 0x01

#define AVTP_CRF_AST  0x01    // Audio Sample Timestamp

#define AF_SR_LEN (sizeof(af_sr) / sizeof(uint32_t))

const uint32_t af_sr[] = {
  0,
  8000,
  16000,
  32000,
  44100,
  48000,
  88200,
  96000,
  176400,
  192000,
};

#pragma pack(push, 1)

typedef struct {
  mac_t dstmac;
  mac_t srcmac;
  uint16_t type;
} eth_t;

typedef struct {
  uint16_t misc;
  uint16_t type;
} vlan_t;

typedef struct {
  uint8_t type;
} avtp_type_t;

typedef struct {
  uint8_t misc1;
  uint8_t seqnr;
  uint8_t type;
  uint64_t sid;
  uint32_t misc2;
  uint16_t length;
  uint16_t interval;
} avtp_crf_t;

typedef struct {
  uint8_t misc1;
  uint8_t seqnr;
  uint8_t misc2;
  uint64_t sid;
  uint32_t ts;
  uint8_t format;
  uint16_t misc3;
  uint8_t bits;
  uint16_t length;
  uint8_t misc4;
  uint8_t misc5;
} avtp_af_t;

#pragma pack(pop)


static inline void debug_mac(mac_t mac)
{
  debug("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

static inline void copymac(mac_t src, mac_t dst)
{
  uint8_t i;
  for (i = 0; i < sizeof(mac_t); i++) dst[i] = src[i];
}

static inline uint16_t vlan_id(vlan_t *vlan)
{
  return ntohs(vlan->misc) & VLAN_ID_MASK;
}

static inline uint32_t crf_base_freq(avtp_crf_t *crf)
{
  return ntohl(crf->misc2) & CRF_BASE_FREQ_MASK;
}

static inline uint16_t af_channels(avtp_af_t *af)
{
  return ntohs(af->misc3) & AF_CHANNELS_MASK;
}

static inline uint8_t af_samplerate(avtp_af_t *af)
{
  return ntohs(af->misc3) >> AF_SR_SHIFT;
}

static inline uint16_t af_chunks(avtp_af_t *af)
{
  return ntohs(af->length) / af_channels(af) / (af->bits / 8);
}

static inline bool af_sid_valid(avtp_af_t *af)
{
  return af->misc1 >> AF_SID_VALID_SHIFT;
}

static inline bool crf_sid_valid(avtp_crf_t *crf)
{
  return crf->misc1 >> CRF_SID_VALID_SHIFT;
}

static inline bool af_ts_valid(avtp_af_t *af)
{
  return af->misc1 & AF_TS_VALID_MASK;
}

static inline bool af_sparse_ts(avtp_af_t *af)
{
  return (af->misc4 >> AF_SPARSE_TS_SHIFT) & AF_SPARSE_TS_MASK;
}


static inline void get_avtp_af_chunks_int16(packet_t packet, uint16_t size, avtp_t *avtp)
{
  int16_t *samples;
  uint16_t chunk, channel, idx;

  if (size < (avtp->channels * avtp->chunks * (avtp->bits / 8))) {
    debug("invalid size!\n");
    return;
  }

  if ((avtp->channels * avtp->chunks) > MAX_INT16_SAMPLES) {
    debug("sample array too small!\n");
    return;
  }

  samples = (int16_t *)packet;

  for (chunk = 0; chunk < avtp->chunks; chunk++) {
    debug("chunk %u samples:", chunk);
    for (channel = 0; channel < avtp->channels; channel++) {
      idx = chunk * avtp->channels + channel;
      avtp->sample_int16[idx] = ntohs(samples[idx]);
      debug(" %04x", (uint16_t)avtp->sample_int16[idx]);
    }
    debug("\n");
  }

  avtp->is_valid = true;
}

static inline void get_avtp_af(packet_t packet, uint16_t size, avtp_t *avtp)
{
  avtp_af_t *avtp_af;

  if (size < sizeof(avtp_af_t)) {
    debug("invalid size!\n");
    return;
  }

  avtp_af = (avtp_af_t *)packet;

  avtp->sid_valid = af_sid_valid(avtp_af);
  debug("stream id valid %u\n", avtp->sid_valid);

  avtp->ts_valid = af_ts_valid(avtp_af);
  debug("timestamp valid %u\n", avtp->ts_valid);

  debug("seqnr %u\n", avtp_af->seqnr);
  avtp->seqnr = avtp_af->seqnr;

  debug("stream id 0x%016" PRIx64 "\n", ntohll(avtp_af->sid));
  avtp->sid = ntohll(avtp_af->sid);

  debug("ts %u\n", ntohl(avtp_af->ts));
  avtp->ts = ntohl(avtp_af->ts);

  avtp->bits = avtp_af->bits;
  debug("bits %u\n", avtp->bits);

  avtp->channels = af_channels(avtp_af);
  debug("channels %u\n", avtp->channels);

  debug("samplerate ");
  if (af_samplerate(avtp_af) < AF_SR_LEN) {
    avtp->freq = af_sr[af_samplerate(avtp_af)];
    debug("%u\n", avtp->freq);
  } else {
    avtp->freq = 0;
    debug("unsupported\n");
  }

  avtp->sparse_ts = af_sparse_ts(avtp_af);
  debug("sparse_ts %u\n", avtp->sparse_ts);

  debug("data len %u\n", ntohs(avtp_af->length));

  avtp->chunks = af_chunks(avtp_af);
  debug("chunks %u\n", avtp->chunks);

  debug("format ");
  if (avtp_af->format == AF_INT16) {
    avtp->format = avtp_af->format;
    debug("int16\n");
    get_avtp_af_chunks_int16(&packet[sizeof(avtp_af_t)], size - sizeof(avtp_af_t), avtp);
  } else {
    debug("unsupported!\n");
  }
}

static inline void get_avtp_crf_ts(packet_t packet, uint16_t length, uint16_t size, avtp_t *avtp)
{
  crf_ts_t *crf_ts;
  uint16_t ts_idx;

  if (size < length) {
    debug("invalid size!\n");
    return;
  }

  crf_ts = (crf_ts_t *)packet;

  avtp->crf_ts_cnt = length / sizeof(crf_ts_t);
  debug("ts count %u\n", avtp->crf_ts_cnt);

  if (avtp->crf_ts_cnt > MAX_CRF_TS_COUNT) {
    debug("ts array too small!\n");
  } else {
    for (ts_idx = 0; ts_idx < avtp->crf_ts_cnt; ts_idx++) {
      debug("ts[%u] 0x%016" PRIx64 "\n", ts_idx, ntohll(crf_ts[ts_idx]));
      avtp->crf_ts[ts_idx] = ntohll(crf_ts[ts_idx]);
    }
  }

  avtp->is_valid = true;
}

static inline void get_avtp_crf(packet_t packet, uint16_t size, avtp_t *avtp)
{
  avtp_crf_t *avtp_crf;

  if (size < sizeof(avtp_crf_t)) {
    debug("invalid size!\n");
    return;
  }

  avtp_crf = (avtp_crf_t *)packet;

  avtp->sid_valid = crf_sid_valid(avtp_crf);
  debug("stream id valid %u\n", avtp->sid_valid);

  debug("seqnr %u\n", avtp_crf->seqnr);
  avtp->seqnr = avtp_crf->seqnr;

  debug("crf type ");
  if (avtp_crf->type == AVTP_CRF_AST) {
    debug("AST\n");

    debug("stream id 0x%016" PRIx64 "\n", ntohll(avtp_crf->sid));
    avtp->sid = ntohll(avtp_crf->sid);

    debug("base freq %u\n", crf_base_freq(avtp_crf));
    avtp->freq = crf_base_freq(avtp_crf);

    debug("interval %u\n", ntohs(avtp_crf->interval));
    avtp->crf_interval =  ntohs(avtp_crf->interval);

    debug("data len %u\n", ntohs(avtp_crf->length));
    get_avtp_crf_ts(&packet[sizeof(avtp_crf_t)], ntohs(avtp_crf->length), size - sizeof(avtp_crf_t), avtp);

  } else {
    debug("unsupported!\n");
  }
}

static inline void get_avtp_type(packet_t packet, uint16_t size, avtp_t *avtp)
{
  avtp_type_t *avtp_type;

  if (size < sizeof(avtp_type_t)) {
    debug("invalid size!\n");
    return;
  }

  avtp_type = (avtp_type_t *)packet;

  avtp->type = avtp_type->type;;

  debug("avtp type ");
  if (avtp_type->type == AVTP_TYPE_AF) {
    debug("AF\n");
    get_avtp_af(&packet[sizeof(avtp_type_t)], size - sizeof(avtp_type_t), avtp);
  } else if (avtp_type->type == AVTP_TYPE_CRF) {
    debug("CRF\n");
    get_avtp_crf(&packet[sizeof(avtp_type_t)], size - sizeof(avtp_type_t), avtp);
  } else {
    debug("unsupported!\n");
  }
}

static inline void get_vlan(packet_t packet, uint16_t size, avtp_t *avtp)
{
  vlan_t *vlan;

  if (size < sizeof(vlan_t)) {
    debug("invalid size!\n");
    return;
  }

  vlan = (vlan_t *)packet;

  debug("vlan id %u\n", vlan_id(vlan));
  avtp->vlan_id = vlan_id(vlan);

  debug("vlan ethtype ");
  if (ntohs(vlan->type) == ETHTYPE_AVTP) {
    debug("AVTP\n");
    avtp->is_avtp = true;
    get_avtp_type(&packet[sizeof(vlan_t)], size - sizeof(vlan_t), avtp);
  } else {
    avtp->is_avtp = false;
    debug("unsupported!\n");
  }
}

static inline void get_eth(packet_t packet, avtp_t *avtp)
{
  eth_t *eth;

  if (avtp->packet_size < sizeof(eth_t)) {
    debug("invalid size!\n");
    return;
  }

  eth = (eth_t *)packet;

  debug("dst mac "); debug_mac(eth->dstmac); debug("\n");
  debug("src mac "); debug_mac(eth->srcmac); debug("\n");
  copymac(eth->dstmac, avtp->dstmac);
  copymac(eth->srcmac, avtp->srcmac);

  debug("ethtype ");
  if (ntohs(eth->type) == ETHTYPE_AVTP) {
    debug("AVTP\n");
    avtp->is_avtp = true;
    avtp->has_vlan = false;
    get_avtp_type(&packet[sizeof(eth_t)], avtp->packet_size - sizeof(eth_t), avtp);
  } else if (ntohs(eth->type) == ETHTYPE_VLAN) {
    debug("VLAN\n");
    avtp->has_vlan = true;
    get_vlan(&packet[sizeof(eth_t)], avtp->packet_size - sizeof(eth_t), avtp);
  } else {
    avtp->is_avtp = false;
    debug("unsupported!\n");
  }
}

void packet2avtp(packet_t packet, header_t *header, avtp_t *avtp)
{
  avtp->packet_size = header->len;
  debug("packet len %u\n", avtp->packet_size);

  avtp->packet_ts = (long double)header->ts.tv_sec + (long double)header->ts.tv_usec / 1000000.0;
  debug("packet ts %ld\n", avtp->packet_ts);

  avtp->is_valid = false;
  get_eth(packet, avtp);

  debug("---\n");
}
