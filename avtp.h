/*
  pcap_avtp_reader
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#pragma once

#include "config.h"
#include <inttypes.h>
#include <stdbool.h>
#include <pcap.h>

#define ETHTYPE_AVTP 0x22f0
#define ETHTYPE_VLAN 0x8100

#define AVTP_TYPE_AF  0x02    // Audio Format
#define AVTP_TYPE_CRF 0x04    // Clock Reference Format

#define MAX_CRF_TS_COUNT 16
#define MAX_INT16_SAMPLES 1024

#define AF_NA    0x00
#define AF_FLT32 0x01
#define AF_INT32 0x02
#define AF_INT24 0x03
#define AF_INT16 0x04

typedef uint8_t mac_t[6];
typedef uint64_t crf_ts_t;

typedef const u_char *packet_t;
typedef struct pcap_pkthdr header_t;

typedef struct {
  long double packet_ts;
  uint16_t packet_size;
  bool is_avtp;
  bool is_valid;
  mac_t dstmac;
  mac_t srcmac;
  bool has_vlan;
  uint16_t vlan_id;
  uint8_t type;
  bool sid_valid;
  bool ts_valid;
  bool sparse_ts;
  uint8_t seqnr;
  uint64_t sid;
  uint32_t freq;
  uint16_t crf_interval;
  uint16_t crf_ts_cnt;
  crf_ts_t crf_ts[MAX_CRF_TS_COUNT];
  uint32_t ts;
  uint8_t bits;
  uint16_t channels;
  uint16_t chunks;
  uint8_t format;
  int16_t sample_int16[MAX_INT16_SAMPLES];
} avtp_t;


void packet2avtp(packet_t packet, header_t *header, avtp_t *avtp);
