#!/bin/bash
#
# pcap_avtp_reader
# Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>
#
# SPDX-License-Identifier: LGPL-2.1-or-later
#

MYDIR=${0%/*}

[ "${1}" = "" ] && echo -e "\nUsage:\n pcap_avtp_list_macs.sh pcap-file" && exit 0

${MYDIR}/pcap_avtp_reader ${1} | grep mac | sort | uniq -c
