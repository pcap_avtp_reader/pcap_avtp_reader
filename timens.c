/*
  pcap_avtp_reader
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>
#include "timens.h"

timens_t get_time()
{
  timens_t ret;

  clock_gettime(CLOCK_MONOTONIC, &ret);

  return ret;
}

// returns (time1 - time2)
timens_t diff_time(timens_t time1, timens_t time2)
{
  timens_t ret;

  if (time2.tv_nsec > time1.tv_nsec) {
    ret.tv_nsec = 1000000000 - time2.tv_nsec + time1.tv_nsec;
    ret.tv_sec = time1.tv_sec - time2.tv_sec - 1;
  } else {
    ret.tv_nsec = time1.tv_nsec - time2.tv_nsec;
    ret.tv_sec = time1.tv_sec - time2.tv_sec;
  }

  return ret;
}

void print_time(timens_t time)
{
  printf("%ld.%09ld",time.tv_sec, time.tv_nsec);
}
