/*
  pcap2w64
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <pcap.h>

#include "timens.h"
#include "avtp.h"
#include "w64.h"

#define safe_snprintf(dest, ...) snprintf(dest, sizeof(dest), __VA_ARGS__)

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define DEFAULT_FILTER "(ether proto " STR(ETHTYPE_VLAN) " or ether proto " STR(ETHTYPE_AVTP) ")"
#define DST_MAC_FILTER "(ether dst %s)"
#define MAX_FILTER_LEN 256

typedef char errbuf_t[PCAP_ERRBUF_SIZE];
typedef struct bpf_program filter_t;

#define PCAP_COMPILE_OPTIMIZE 1

void usage()
{
  fprintf(stderr, "\nUsage:\n pcap2w64 pcap-file w64-file [destination-mac]\n");
}

inline void print_mac(mac_t mac)
{
  printf("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

bool store_audio(avtp_t *avtp, FILE *w64)
{
  static uint64_t sid = 0;
  static bool first_run = true;

  if (avtp->is_valid) {
    if (avtp->type != AVTP_TYPE_AF) return true;  // ignore CRF streams
    if ((!first_run) && (avtp->sid != sid)) {
      fprintf(stderr, "Error: Multiple audio streams found! Use the destination MAC filter. Exiting.\n");
      return false;
    }
  }

  if (!avtp->is_valid) {
    fprintf(stderr, "Warning: Invalid avtp data found! Ignoring.\n");
    return true;
  }

  //printf("seqnr %u\n", avtp->seqnr);
 
  if (avtp->format == AF_INT16) {
    if (first_run) {
      first_run = false;
      sid = avtp->sid;
      if (!write_w64_header(w64, 0, avtp->channels, avtp->freq, avtp->bits)) { // write W64 header placeholder
        fprintf(stderr, "Error: Write failed!");
        return false;
      }
    }
    if (fwrite(avtp->sample_int16, (avtp->chunks * avtp->channels * (avtp->bits / 8)), 1, w64) != 1) {
      fprintf(stderr, "Error: Write failed!");
      return false;
    }
  } else {
    printf("Error: Unsupported format!");
  }

  return true;
}

int main(int argc, char **argv)
{
  pcap_t *pcap;
  avtp_t avtp;
  errbuf_t errbuf;
  filter_t filter;
  packet_t packet;
  header_t header;
  timens_t time1, time2;
  char filter_str[MAX_FILTER_LEN];
  FILE *w64;
  uint64_t packet_cnt = 0;

  // Check the number of arguments
  if ((argc < 3) || (argc > 4)) {
    usage();
    exit(EXIT_FAILURE);
  }

  if (argc == 3) {
    safe_snprintf(filter_str, DEFAULT_FILTER);
  } else {
    safe_snprintf(filter_str, DEFAULT_FILTER " and " DST_MAC_FILTER, argv[3]);
  }

  // Open the PCAP file
  pcap = pcap_open_offline(argv[1], errbuf);

  if (!pcap) {
    fprintf(stderr, "%s\n", errbuf);
    exit(EXIT_FAILURE);
  }

  // Compile the filter
  if (pcap_compile(pcap, &filter, filter_str, PCAP_COMPILE_OPTIMIZE, PCAP_NETMASK_UNKNOWN) == PCAP_ERROR) {
    pcap_close(pcap);
    fprintf(stderr, "Error: Invalid filter %s: %s\n", filter_str, pcap_geterr(pcap));
    exit(EXIT_FAILURE);
  }

  // Apply the filter
  if (pcap_setfilter(pcap, &filter) == PCAP_ERROR) {
    pcap_close(pcap);
    fprintf(stderr, "Error: Filter failed %s: %s\n", filter_str, pcap_geterr(pcap));
    exit(EXIT_FAILURE);
  }

  // Open the W64
  w64 = fopen(argv[2], "wb");
  if (!w64) {
    pcap_close(pcap);
    fprintf(stderr, "Error: Unable to open the output file for writing!\n");
    exit(EXIT_FAILURE);
  }

  time1 = get_time();

  while (1) {
    // Read packet
    packet = pcap_next(pcap, &header);

    if (!packet) break;

    packet_cnt++;

    // Convert packet data to avtp structure
    packet2avtp(packet, &header, &avtp);

    // Store the audio data
    if (!store_audio(&avtp, w64)) {
      fclose(w64);
      pcap_close(pcap);
      exit(EXIT_FAILURE);
    }
  };

  time2 = get_time();

  // Re-write the W64 header with correct length
  if (!write_w64_header(w64, ftell(w64), avtp.channels, avtp.freq, avtp.bits)) {
    fprintf(stderr, "Error: Write failed!");
  }

  // Close the W64 file
  fclose(w64);

  // Close the PCAP file
  pcap_close(pcap);

  // Print stats
  printf("Total time "); print_time(diff_time(time2, time1)); printf("s\n");
  printf("Packets processed %ld\n", packet_cnt);

  exit(EXIT_SUCCESS);
}
