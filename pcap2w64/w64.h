/*
  pcap2w64
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#pragma once

#include "config.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

bool write_w64_header(FILE *w64, uint64_t file_size, uint16_t channels, uint32_t freq, uint16_t bits);
