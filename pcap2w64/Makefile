#
# pcap2w64
# Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>
#
# SPDX-License-Identifier: LGPL-2.1-or-later
#

APPNAME=pcap2w64
CC=gcc --std=gnu99
CFLAGS=-O2
BINDIR=/usr/bin
MANDIR=/usr/share/man/man1
MANPAGE=$(APPNAME).1
INCDIR=../
LIBS=-lpcap
OBJS=../timens.o ../avtp.o w64.o
NAUGHTY_HEADERS=config.h

$(APPNAME): $(APPNAME).c $(OBJS)
	$(CC) -Wall -pedantic $(CFLAGS) $(LDFLAGS) -I$(INCDIR) -o $(APPNAME) $(APPNAME).c $(OBJS) $(LIBS)

%.o: %.c %.h $(NAUGHTY_HEADERS)
	$(CC) -Wall -pedantic $(CFLAGS) $(LDFLAGS) $(LIBS) -c $< -o $@

.PHONY: install
install:
	install -Dpm755 $(APPNAME) $(DESTDIR)/$(BINDIR)/$(APPNAME)
	install -Dpm644 $(MANPAGE) $(DESTDIR)/$(MANDIR)/$(MANPAGE)

.PHONY: clean
clean:
	rm -f $(APPNAME)
	rm -f *.o
