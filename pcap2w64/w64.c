/*
  pcap2w64
  Copyright (C) 2023 Jaromir Capik <jaromir.capik@email.cz>

  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "w64.h"

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
  #error "Big endian not supported (yet)!"
#endif

const uint8_t w64_riff[] = {0x72, 0x69, 0x66, 0x66, 0x2E, 0x91, 0xCF, 0x11,
                            0xA5, 0xD6, 0x28, 0xDB, 0x04, 0xC1, 0x00, 0x00};

const uint8_t w64_wave[] = {0x77, 0x61, 0x76, 0x65, 0xF3, 0xAC, 0xD3, 0x11,
                            0x8C, 0xD1, 0x00, 0xC0, 0x4F, 0x8E, 0xDB, 0x8A};

const uint8_t w64_fmt[]  = {0x66, 0x6D, 0x74, 0x20, 0xF3, 0xAC, 0xD3, 0x11,
                            0x8C, 0xD1, 0x00, 0xC0, 0x4F, 0x8E, 0xDB, 0x8A};

const uint8_t w64_data[] = {0x64, 0x61, 0x74, 0x61, 0xF3, 0xAC, 0xD3, 0x11,
                            0x8C, 0xD1, 0x00, 0xC0, 0x4F, 0x8E, 0xDB, 0x8A};

bool write_w64_header(FILE *w64, uint64_t file_size, uint16_t channels, uint32_t freq, uint16_t bits)
{
  uint16_t fmt_tag = 0x01; // PCM
  uint32_t bytes_per_second = freq * channels * (bits / 8);
  uint16_t block_align = channels * ((bits + 7) / 8); 
  uint64_t fmt_size = sizeof(w64_fmt) + sizeof(fmt_size) + sizeof(fmt_tag) + sizeof(channels)
                    + sizeof(freq) + sizeof(bytes_per_second) + sizeof(block_align) + sizeof(bits);
  uint64_t data_size = file_size - sizeof(w64_riff) - sizeof(file_size) - sizeof(w64_wave) - fmt_size;

  if (fseek(w64, 0, SEEK_SET)) return false;

  if (fwrite(w64_riff, sizeof(w64_riff), 1, w64) != 1) return false;
  if (fwrite(&file_size, sizeof(file_size), 1, w64) != 1) return false;
  if (fwrite(w64_wave, sizeof(w64_wave), 1, w64) != 1) return false;
  if (fwrite(w64_fmt, sizeof(w64_fmt), 1, w64) != 1) return false;
  if (fwrite(&fmt_size, sizeof(fmt_size), 1, w64) != 1) return false;
  if (fwrite(&fmt_tag, sizeof(fmt_tag), 1, w64) != 1) return false;
  if (fwrite(&channels, sizeof(channels), 1, w64) != 1) return false;
  if (fwrite(&freq, sizeof(freq), 1, w64) != 1) return false;
  if (fwrite(&bytes_per_second, sizeof(bytes_per_second), 1, w64) != 1) return false;
  if (fwrite(&block_align, sizeof(block_align), 1, w64) != 1) return false;
  if (fwrite(&bits, sizeof(bits), 1, w64) != 1) return false;
  if (fwrite(w64_data, sizeof(w64_data), 1, w64) != 1) return false;
  if (fwrite(&data_size, sizeof(data_size), 1, w64) != 1) return false;

  return true;
}
